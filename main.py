# #
# 
# main.py
#
# Main controlller for home_miner
# - handles web interface for monitoring and adjusting config
# - handles interfacing with Arduino for Alexa control
#
# Copyright 2018 Michael Kukar
# 
# #

import time
import sys
import os
import json
import threading
import logging
from tkinter import Tk, Button, StringVar, Label, Toplevel
import datetime

# aws API for the shadow device
from AWSIoTPythonSDK.MQTTLib import AWSIoTMQTTShadowClient

from home_miner import HomeMiner
from arduino import Arduino
from config_handler import ConfigHandler
import nicehash_handler
import btc_info_module
from config_window import ConfigWindow

class Gui:

    # #
    # PUBLIC VARIABLES
    # #

    # #
    # PRIVATE VARIABLES
    # #

    # setup objects
    logger = None
    arduino = None
    config = None
    homeMiner = None
    daemon = None
    shadowClient = None

    # text objects
    toggle_stop_text = None
    status_text = None
    profit_text = None
    balance_text = None

    # #
    # CONSTRUCTOR
    # #

    def __init__(self, master):
        self.root = master
        # sets up the associate objects
        self.setup()

        # sets up gui display for the main window
        master.title("HOME MINER")
        master.configure(bg = "white")
        master.resizable(width = False, height = False)

        # variables
        self.toggle_stop_text = StringVar()
        self.status_text = StringVar()
        self.profit_text = StringVar()
        self.balance_text = StringVar()

        # status label
        self.status_label = Label(master, textvariable = self.status_text, foreground = "black", bg = 'light blue', font = "helvetica 11", padx = 5, pady = 5)
        self.status_label.grid(row=0,column=0, columnspan=2, sticky = 'news')
        self.status_text.set("STATUS: Mining")
        # profit label
        self.profit_label_top = Label(master, text="EST PROFIT USD/Day", foreground = "white", bg = 'green', font = "helvetica 11", padx = 5, pady = 5)
        self.profit_label_top.grid(row=1, column = 0)
        self.profit_label = Label(master, textvariable = self.profit_text, foreground = 'black', bg = 'white', font = "helvetica 11 bold", padx = 5, pady = 5)
        self.profit_label.grid(row=2,column=0, sticky = 'news')
        self.profit_text.set("$0.00")
        # balance label
        self.balance_label_top = Label(master, text = "BALANCE BTC (USD)", bg = 'orange', foreground = 'white', font = "helvetica 11", padx = 5, pady = 5)
        self.balance_label_top.grid(row = 1, column = 1, sticky = 'news')
        self.balance_label = Label(master, textvariable = self.balance_text, bg = 'white', font = "helvetica 11 bold", padx = 5, pady = 5)
        self.balance_label.grid(row = 2, column = 1, sticky = 'news')
        self.balance_text.set("1.0 ($1000.00)")
        # settings button
        self.settings_button = Button(master, text = "Settings", command = self.open_config_window, font = "helvetica 11", padx = 5, pady = 5)
        self.settings_button.grid(row = 0, column = 2, sticky = "NEWS")
        # stop/resume button
        self.toggle_stop_button = Button(master, textvariable = self.toggle_stop_text, command = self.toggle_stop, bg = "dark gray", font = "helvetica 11", state = "disabled")
        self.toggle_stop_button.grid(row=1,column = 2, rowspan = 2, columnspan = 2, sticky = 'NEWS')
        self.toggle_stop_text.set("STOP")

        # updates gui
        self.update_gui()

        # writes initial state to shadow        
        self.shadow_request_mining_state(1)
        self.shadow_write_mining_state(1)


    # #
    # DESTRUCTOR
    # #

    def on_closing(self):
        self.logger.info("Exiting...")
        self.homeMiner.force_stop()
        self.homeMiner.kill_daemon()
        self.root.destroy()


    # #
    # PUBLIC FUNCTIONS
    # #


    '''
    Toggles between stopping and resuming mining

    return : none
    '''
    def toggle_stop(self):
        if self.toggle_stop_text.get() == "STOP":
            self.homeMiner.force_stop()
            self.toggle_stop_text.set("RESUME")
            self.toggle_stop_button.configure(bg = "green")
            self.status_text.set("STATUS: Stopped")
            self.shadow_request_mining_state(0)
            self.shadow_write_mining_state(0)
            self.logger.info("Stopping mining.")
        else:
            self.homeMiner.reset()
            self.toggle_stop_text.set("STOP")
            self.toggle_stop_button.configure(bg = "red")
            self.status_text.set("STATUS: Not mining")
            self.shadow_request_mining_state(1)
            self.shadow_write_mining_state(1)
            self.logger.info("Resuming mining.")



    '''
    Updates every few seconds to ensure the GUI is up-to-date

    return : none
    '''
    def update_loop(self):
        try:
            self.update_gui()
        except Exception as e:
            self.logger.error(e)
            self.logger.error("Failed to update gui")
        self.root.after(15000, self.update_loop)

    '''
    Checks status and updates labels

    return : none
    '''
    def update_gui(self):
        # status & start-stop button
        if nicehash_handler.is_nicehash_miner_open():
            self.status_text.set("STATUS: Mining")
            self.toggle_stop_text.set("STOP")
            self.toggle_stop_button.configure(bg = "red", state = "normal")
        elif self.homeMiner.get_force_off():
            self.status_text.set("STATUS: Stopped")
            self.toggle_stop_text.set("RESUME")
            self.toggle_stop_button.configure(bg = "green", state = "normal")
        else:
            self.status_text.set("STATUS: Not mining")
            self.toggle_stop_text.set("STOP")
            self.toggle_stop_button.configure(bg = "red", state = "normal")

        # profit
        curProfit = (btc_info_module.get_current_btc_value() * nicehash_handler.get_maximum_profitability_current())
        self.profit_text.set("$" + format(curProfit, '.2f'))
        if curProfit >= 0:
            self.profit_label.configure(foreground="green")
            self.profit_label_top.configure(bg = "light green", foreground = "black")
        else:
            self.profit_label.configure(foreground="red")
            self.profit_label_top.configure(bg = "orange red", foreground = "white")

        # balance
        curBalanceDict = nicehash_handler.get_current_bitcoin_balance(self.config.get_api_id(), self.config.get_api_key(), self.config.get_btc_address())
        if curBalanceDict is not None:
            total_balance = 0.0
            for entry in curBalanceDict:
                total_balance += float(curBalanceDict[entry])
            total_balance_usd = total_balance * btc_info_module.get_current_btc_value()
        self.balance_text.set(format(total_balance, '.8f') + " ($" + format(total_balance_usd, '.2f') + ")")


    '''
    Sets up required subclasses (arduino, home miner daemon, etc.)

    return : none
    '''
    def setup(self):
        # sets up logging
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(logging.INFO)
        ch = logging.StreamHandler() # XXX TODO - move from console to file after debugging fully complete
        ch.setLevel(logging.DEBUG)
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        ch.setFormatter(formatter)
        self.logger.addHandler(ch)

        # arduino
        self.logger.info("Connecting to arduino...")
        self.arduino = Arduino()
        if (self.arduino.initialized):
            self.logger.info("Connected.")
        else:
            self.logger.warn("Failed.")

        # config file handler
        self.logger.info("Connecting to config file...")
        self.config = ConfigHandler()

        # daemon
        self.logger.info("Starting daemon...")
        self.homeMiner = HomeMiner(self.arduino, self.config)
        self.daemon = threading.Thread(target=self.homeMiner.daemon)
        self.daemon.start()


        # shadow client
        self.logger.info("Setting up IoT shadow...")
        try:
            # NOTE - all hard-coded for some slight security (not in config file)
            self.shadowClient = AWSIoTMQTTShadowClient("home_miner")
            self.shadowClient.configureEndpoint("a1dmihzayp9e6c.iot.us-east-1.amazonaws.com", 8883)
            self.shadowClient.configureCredentials(os.getcwd() + '\\certificates\\root-CA.crt.txt', os.getcwd() + "\\certificates\\399bb7d644-private.pem.key" , os.getcwd() + "\\certificates\\399bb7d644-certificate.pem.crt")
            self.shadowClient.configureConnectDisconnectTimeout(10) # 10 secs
            self.shadowClient.configureMQTTOperationTimeout(5) # 5 secs
            self.shadowClient.configureAutoReconnectBackoffTime(1,32,20)
            self.shadowClient.connect()

            # shadow device
            self.shadowDevice = self.shadowClient.createShadowHandlerWithName("home_miner", True)
            self.shadowDevice.shadowRegisterDeltaCallback(self.shadow_get)
            time.sleep(5)
        except Exception as e:
            self.logger.error("Failed to set up IoT shadow. Will not have Alexa skill integration until restarted.")
            self.logger.error(e)

        self.logger.info("Setup complete.")

    '''
    Handles changes to the shadow calling commands (deltas)

    param payload        : json payload string
    param responseStatus : response category (timeout, accepted, delta, etc.)
    param token          : unique identifier token from sender

    return               : none
    '''
    def shadow_get(self, payload, responseStatus, token):
        #print("GOT UPDATE:")
        #print(responseStatus)
        #print(json.loads(payload))
        # extracts the mining state
        jsonDict = json.loads(payload)
        newState = -1
        if 'state' in jsonDict and 'mining' in jsonDict['state']:
            # new state is
            #print("NEW STATE:" + str(jsonDict['state']['mining']))
            newState = int(jsonDict['state']['mining'])

        # updates mining status
        if newState == 1:
            # mining should be ON (resumed)
            if self.toggle_stop_text.get() == "RESUME":
                self.toggle_stop()
        elif newState == 0:
            if self.toggle_stop_text.get() == "STOP":
                self.toggle_stop()


    '''
    Sets the reported state (what it actyally is)

    param state : 1 or 0 (1 for normal, 0 for stopped)

    return      : true on success, false on fail/invalid input
    return type : bool
    '''
    def shadow_write_mining_state(self, state):
        # state can be 0 or 1
        if state != 0 and state != 1:
            return False
        # build JSON
        jsonStr = '{"state":{"reported":{"mining":' + str(state) + '}}}'
        try:
            self.shadowDevice.shadowUpdate(jsonStr, self.debugger, 5)
        except Exception as e:
            self.logger.error("Failed to update IoT shadow.")
            self.logger.error(e)
            return False
        return True

    '''
    Sets the desired state (what it SHOULD be)

    param state : 1 or 0 (1 for normal, 0 for stopped)

    return      : true on success, false on fail/invalid input
    return type : bool
    '''
    def shadow_request_mining_state(self, state):
        # state can be 0 or 1
        if state != 0 and state != 1:
            return False
        # build JSON
        jsonStr = '{"state":{"desired":{"mining":' + str(state) + '}}}'
        try:
            self.shadowDevice.shadowUpdate(jsonStr, self.debugger, 5)
        except Exception as e:
            self.logger.error("Failed to update IoT shadow.")
            self.logger.error(e)
            return False
        return True

    '''
    Dummy handler for debugging purposes

    return : none
    '''
    def debugger(self, payload, responseStatus, token):
        #print(responseStatus)
        #if payload is not None:
        #    print(payload)
        pass


    '''
    Opens config window
    
    return : none
    '''
    def open_config_window(self):
        self.config_window = Toplevel()
        self.config_frame = ConfigWindow(self.config_window)


 
if __name__ == "__main__":
    print("Home Miner - DEV VERSION")
    print("Copyright 2018 Michael Kukar")

    # starts gui
    root = Tk()
    gui = Gui(root)
    root.protocol("WM_DELETE_WINDOW", gui.on_closing) # sends close command when window closed
    root.after(15000, gui.update_loop)
    root.mainloop()
