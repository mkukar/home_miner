# #
# 
# arduino.py
#
# Handles interfacing with the Arduino MKR1000 board over UDP
#
# Copyright 2018 Michael Kukar
# 
# #

from socket import *
import logging
import threading


class Arduino:

    # #
    # PUBLIC VARIABLES
    # #

    SERVER_PORT = 12121
    CLIENT_PORT = 2390

    initialized = False

    # #
    # PRIVATE VARIABLES
    # #
    _SOCKET = None
    _PORT = 12003
    _ARDUINO_ADDR = None
    _ARDUINO_PORT = 2390

    _lock = threading.Lock()

    # Handshake messages
    _SEARCH_MESSAGE = "ARDUINO_SEARCH"
    _RECIEVE_SEARCH_MESSAGE = "ARDUINO_FOUND"

    # temperature messages
    _TEMP_QUERY_HEADER = "ARDUINO_TEMP_" # ARDUINO_TEMP_C or ARDUINO_TEMP_F
    _TEMP_RESPONSE_HEADER = "TEMP_" # TEMP_###_F e.g. TEMP_45_F or TEMP_10_C C = celcius F = farenheit

    # power messages
    _POWER_QUERY = "ARDUINO_POWER"
    _POWER_RESPONSE_HEADER = "POWER_" # POWER_### e.g. POWER_145 in Watts


    # enums
    FARENHEIT = "F"
    CELCIUS = "C"

    # #
    # CONSTRUCTOR
    # #

    def __init__(self):
        # sets up logger
        self._logger = logging.getLogger(__name__)
        self._logger.setLevel(logging.INFO)
        ch = logging.StreamHandler() # XXX TODO - move from console to file after debugging fully complete
        ch.setLevel(logging.DEBUG)
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        ch.setFormatter(formatter)
        self._logger.addHandler(ch)

        # attempts to connect to the arduino
        if not self.connect():
            self._logger.error("Failed to connect to arduino!")
        else:
            self._logger.info("Connected to arduino successfully.")
            self.initialized = True


    # #
    # PUBLIC FUNCTIONS
    # #

    '''
    Returns the current power usage in watts

    param timeout : (optional) how many seconds until timeout

    return        : power usage in watts, or -1 on error
    return type   : int
    '''
    def get_current_power_usage(self, timeout = 10):
        try:
            result = self._send_message(self._POWER_QUERY, timeout = timeout)

            if result is not None:
                watts = int(result.split('_')[-1])
                return watts
            else:
                return -1
        except Exception as e:
            self._logger.warn("Failed to get current power usage.")
            self._logger.warn(e)
            return -1


    '''
    Returns the current temperature in celcius or farenheit

    param units   : (optional) farenheit or celcius, default is farenheit
    param timeout : (optional) how many seconds until timeout

    return        : temperature in celcius or farenheit
    return type   : float
    '''
    def get_current_temperature(self, units = FARENHEIT, timeout = 10):
        # builds query string
        queryString = self._TEMP_QUERY_HEADER + units
        try:
            result = self._send_message(queryString, timeout = timeout)

            if result is not None:
                temp = float(result.split('_')[-1])
                return temp
            else:
                return -1.0
        except Exception as e:
            self._logger.warn("Failed to get current temperature.")
            self._logger.warn(e)
            return -1.0


    '''
    Looks for the arduino's search message and connects to it over UDP

    param timeout : how many seconds until timeout

    return        : true on connected, false on fail
    return type   : bool
    '''
    def connect(self, timeout = 5):
        with self._lock:
            # sets up socket
            self._SOCKET = socket(AF_INET, SOCK_DGRAM)
            bound = False
            while not bound:
                try:
                    self._SOCKET.bind(('', self._PORT))
                except Exception as e:
                    # tries 1 higher port
                    self._PORT += 1
                    continue
                bound = True

            # sets up broadcast and multicast requirements
            self._SOCKET.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
            self._SOCKET.setsockopt(SOL_SOCKET, SO_BROADCAST, 1)

            # sends general message to arduinos broadcast every 5 seconds
            self._SOCKET.settimeout(5)
            second_counter = int(timeout/5)
            while (second_counter > 0):
                # sends message broadcast
                self._SOCKET.sendto(self._SEARCH_MESSAGE.encode(), ('255.255.255.255', self._ARDUINO_PORT))

                second_counter -= 1
                # tries to get a message
                try:
                    message, address = self._SOCKET.recvfrom(1024)
                    if message.decode() == self._RECIEVE_SEARCH_MESSAGE:
                        self._logger.info("Found ardino on address " + str(address))
                        self._ARDUINO_ADDR = address
                        return True
                except TimeoutError:
                    self._logger.info("Timeout met. Trying again...")   
                    continue
                except Exception as e:
                    self._logger.info(e)
                    continue
            return False

    # #
    # PRIVATE FUNCTIONS
    # #

    '''
    Sends a message and returns what it receives

    param message : message string to send
    param timeout : (optional) how many seconds until timeout 

    return        : message recieved, or None on failure
    return type   : string, or None on failure
    '''
    def _send_message(self, message, timeout = 30):
        with self._lock:
            # error checking
            if (self._SOCKET is None or self._ARDUINO_ADDR is None):
                self._logger.error("Cannot send message as arduino not yet discovered.")
                return None

            # sends general message to arduinos broadcast every 5 seconds
            self._SOCKET.settimeout(5)
            second_counter = int(timeout/5)
            while (second_counter > 0):
                # sends message broadcast
                self._SOCKET.sendto(message.encode(), self._ARDUINO_ADDR)

                second_counter -= 1
                # tries to get a message
                try:
                    message, address = self._SOCKET.recvfrom(self._ARDUINO_PORT)
                    return message.decode()
                except TimeoutError:
                    self._logger.info("Timeout met. Trying again...")   
                    continue
                except Exception as e:
                    self._logger.info(e)
                    continue
            return None

