# #
# 
# btc_info_module.py
#
# Handles getting information about bitcoin (pricing, etc.)
#
#
# Copyright 2018 Michael Kukar
# 
# #

import urllib
import urllib.request
import json
import logging

# #
# PUBLIC VARIABLES
# #

# see https://www.coindesk.com/api/
COINDESK_BASE_URL = r"http://api.coindesk.com/v1/bpi"
CURRENT_PRICE_URL = "currentprice"

# #
# PRIVATE VARIABLES
# #

# sets up logger
_logger = logging.getLogger(__name__)
_logger.setLevel(logging.INFO)
ch = logging.StreamHandler() # XXX TODO - move from console to file after debugging fully complete
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
_logger.addHandler(ch)


# #
# PUBLIC FUNCTIONS
# #

'''
Gets the current value of btc in the given currency (default USD)
See - https://www.coindesk.com/api/

param currency_code : (optional) currency code to get value in (default USD)

return              : value of bitcoin in given currency, or None on fail
return type         : float
'''
def get_current_btc_value(currency_code = "USD"):
    # queries coindesk API to get current value
    try:
        request_url = COINDESK_BASE_URL + "/" + CURRENT_PRICE_URL + "/" + currency_code + ".json"
        request_url = r'http://api.coindesk.com/v1/bpi/currentprice/USD.json'
        headers = {'User-Agent' : 'Mozilla'}
        request = urllib.request.Request(request_url,None,headers)
        response = urllib.request.urlopen(request).read()
        resultDict = json.loads(response) # parses to dict
        # extracts value and returns to user
        if "bpi" in resultDict and "USD" in resultDict["bpi"]:
            resultVal = float(resultDict['bpi']['USD']['rate_float'])
            return resultVal
    except urllib.error.HTTPError as e:
        _logger.error("Failed to get from api using url " + request_url)
        _logger.error("Response Code: " + str(e.code) + " Message: " + str(e))
        return None
    except Exception as e:
        _logger.error("Failed to get from api using url " + request_url)
        _logger.error(e)
        return None

