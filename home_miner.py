# #
# 
# home_miner.py
#
# daemon-esque controller for alexa-arduino home cryptocurrency mining project
#
#
# Copyright 2018 Michael Kukar
# 
# #

import datetime
import logging
import threading

import nicehash_handler
import btc_info_module
from config_handler import ConfigHandler
from arduino import Arduino


class HomeMiner:

    # #
    # PUBLIC VARIABLES
    # #

    # #
    # PRIVATE VARIABLES
    # #

    # classes that are initialized in constructor
    _arduino = None
    _config = None
    _logger = None

    _nicehashMinerOn = nicehash_handler.is_nicehash_miner_open()
    _isBlackout = False
    _curMiningTime = 0 # keeps track of minutes mining

    _lock = threading.Lock()
    _forceOn = False
    _forceOff = False

    _killed = False # if true, stops while loop

    # #
    # CONSTRUCTOR
    # #

    def __init__(self, arduino, config_handler):
        # saves variables
        self._arduino = arduino
        self._config = config_handler

        # sets up logging
        self._logger = logging.getLogger(__name__)
        self._logger.setLevel(logging.INFO)
        ch = logging.StreamHandler() # XXX TODO - move from console to file after debugging fully complete
        ch.setLevel(logging.DEBUG)
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        ch.setFormatter(formatter)
        self._logger.addHandler(ch)

    # #
    # PUBLIC FUNCTIONS
    # #

    '''
    Main daemon loop that runs continuously. Run in thread

    return : none
    '''
    def daemon(self):
        while(not self._killed):
            # gets current time
            curTime = datetime.datetime.now()

            # checks nicehash status in case it crashed
            if nicehash_handler.is_nicehash_miner_open() != self._nicehashMinerOn:
                self._logger.warn("Nicehash miner was closed or opened outside of this program. Possible crash or reboot.")
                self._nicehashMinerOn = nicehash_handler.is_nicehash_miner_open()

            # checks if set force on or off as this is an immediate command
            if self.get_force_on() and not self._nicehashMinerOn:
                self._logger.info("Forcing mining on...")
                nicehash_handler.start_nicehash()
                self._nicehashMinerOn = True
                self._curMiningTime = 0
            if self.get_force_off() and self._nicehashMinerOn:
                self._logger.info("Forcing mining off...")
                nicehash_handler.stop_nicehash()
                self._nicehashMinerOn = False
                self._curMiningTime = 0

            # executes changes only once a minute
            if curTime.second == 0:
                # entire block in try statement, so if failure it just waits until the next minute to try again
                try:
                    # refreshes all data
                    self._config.load_data()
                    if not self._arduino.initialized:
                        self._arduino.initialized = self._arduino.connect()

                    # if mining currently, adds 1 minute to mining time
                    if self._nicehashMinerOn:
                        self._curMiningTime += 1

                    cur_balance_dict = nicehash_handler.get_current_bitcoin_balance(
                            api_id = self._config.get_api_id(), 
                            api_key = self._config.get_api_key(), 
                            btc_addr = self._config.get_btc_address())

                    if cur_balance_dict is None:
                        self._logger.warn("Failed to fetch current btc balance information.")
                    else:
                        total_balance = 0.0
                        for entry in cur_balance_dict:
                            total_balance += float(cur_balance_dict[entry])

                        self._logger.info("Current balance: " + str(total_balance) + "BTC")

                    # checks if forced off, if so we just continue
                    with self._lock:
                        if self._forceOff:
                            self._logger.info("Stopped.")
                            continue

                    # checks if blackout time
                    self._isBlackout = self._is_blackout_time(self._config.get_mining_blackout_times())
                    if self._isBlackout and self._nicehashMinerOn:
                        self._logger.info("Stopping nicehash miner for blackout...")
                        nicehash_handler.stop_nicehash()
                        self._nicehashMinerOn = False
                    elif self._isBlackout:
                        self._logger.info("Blackout time.")
                    elif not self._isBlackout:
                        # otherwise checks if profitable
     
                        # gets current profitability
                        cur_hourly_profit = self._calculate_current_profit()
                        self._logger.info("Current profitability is " + str(cur_hourly_profit) + ' USD/hour (' + str(cur_hourly_profit * 24.0) + ' USD/day)')

                        # if profitable and miner is off, we start it
                        if cur_hourly_profit > self._config.get_minimum_profitability() and not self._nicehashMinerOn:
                            self._logger.info("Starting nicehash miner...")
                            nicehash_handler.start_nicehash()
                            self._nicehashMinerOn = True
                        # must be unprofitable and the minimum mining time reached (cannot quickly flip between on and off constantly)
                        elif cur_hourly_profit < self._config.get_minimum_profitability() and self._nicehashMinerOn and self._curMiningTime >= self._config.get_minimum_mining_time():
                            self._logger.info("Stopping nicehash miner...")
                            nicehash_handler.stop_nicehash()
                            self._nicehashMinerOn = False
                            self._curMiningTime = 0
                except Exception as e:
                    self._logger.warn(e)


    '''
    Sets the forceOn to true and forceOff to false to enable mining
    '''
    def force_mining(self):
        with self._lock:
            self._forceOn = True
            self._forceOff = False


    '''
    Sets the forceOn to false and forceOff to true to disable mining
    '''     
    def force_stop(self):
        with self._lock:
            self._forceOn = False
            self._forceOff = True


    '''
    Resets to normal conditions
    '''
    def reset(self):
        self._logger.info("Resetting miner to normal operation")
        with self._lock:
            self._forceOn = False
            self._forceOff = False


    '''
    Ends daemon loop
    '''
    def kill_daemon(self):
        self._logger.info("Killing daemon...")
        with self._lock:
            self._killed = True


    # #
    # PRIVATE FUNCTIONS
    # #


    '''
    Fetches the forceOn boolean using locks
    '''
    def get_force_on(self):
        with self._lock:
            return self._forceOn


    '''
    Fetches the forceStop boolean using locks
    '''
    def get_force_off(self):
        with self._lock:
            return self._forceOff


    '''
    Determines if profitable to mine currently

    return               : current profitability in USD/hour
    return type          : float
    '''
    def _calculate_current_profit(self):
        # gets current hourly profit from mining
        cur_hourly_profit = (btc_info_module.get_current_btc_value() * nicehash_handler.get_maximum_profitability_current()) / 24.0

        # gets current electricity cost
        cur_power_usage = self._arduino.get_current_power_usage() / 1000.0 # convert to kWh
        if cur_power_usage == -1:
            # backup is to get power usage from config file
            cur_power_usage = self.config.get_estimated_power_usage()
        cur_power_price = cur_power_usage * self._config.get_current_power_price()

        # current hourly profit
        cur_hourly_profit -= cur_power_price

        return cur_hourly_profit


    '''
    Determines if current time is inside the blackout time

    param blackout_time_list : list of blackout times from config handler

    return                   : true if blackout time, false if not
    return type              : bool
    '''
    def _is_blackout_time(self, blackout_time_list):
        # gets current time
        curTime = datetime.datetime.now()

        # checks if inside a given blackout time
        isBlackout = False
        for blackout_time in blackout_time_list:
            blackoutStart = blackout_time[0].split(':')
            blackoutEnd = blackout_time[1].split(':')
            # wrap around case
            if int(blackoutEnd[0]) < int(blackoutStart[0]) or (int(blackoutStart[0]) == int(blackoutEnd[0]) and int(blackoutEnd[1]) < int(blackoutStart[1])):
                if curTime.hour > int(blackoutStart[0]) or (curTime.hour == int(blackoutStart[0]) and curTime.minute >= int(blackoutStart[1])):
                    isBlackout = True
                if curTime.hour < int(blackoutEnd[0]) or (curTime.hour == int(blackoutEnd[0]) and curTime.minute <= int(blackoutEnd[1])):
                    isBlackout = True

            else:
                if curTime.hour > int(blackoutStart[0]) or (curTime.hour == int(blackoutStart[0]) and curTime.minute >= int(blackoutStart[1])):
                    if curTime.hour < int(blackoutEnd[0]) or (curTime.hour == int(blackoutEnd[0]) and curTime.minute <= int(blackoutEnd[1])):
                        isBlackout = True

        return isBlackout
