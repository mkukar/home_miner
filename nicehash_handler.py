# #
# 
# nicehash_handler.py
#
# Handles interacting with the nicehash API along with restarting the program in Windows 10
#
#
# Copyright 2018 Michael Kukar
# 
# #

import os
import subprocess
import logging
import urllib
import urllib.request
import json
import math

from pprint import pprint # used only for debugging

# #
# PRIVATE VARIABLES
# #

# sets up logger
_logger = logging.getLogger(__name__)
_logger.setLevel(logging.INFO)
ch = logging.StreamHandler() # XXX TODO - move from console to file after debugging fully complete
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
_logger.addHandler(ch)


# #
# PUBLIC VARIABLES
# #

# API variables (see https://www.nicehash.com/doc-api or https://api.nicehash.com/?p=api)
API_BASE_URL = "https://api.nicehash.com/api?"
BALANCE_METHOD = "balance"
STATS_METHOD = "stats.provider"
CURRENT_STATS_METHOD = "stats.global.current"
TWENTYFOUR_STATS_METHOD = "stats.global.24h"

# maps algorithm numbers from the API to their string name and btc power (e.g. if btc power is in THz, then power = 12)
ALGORITHM_NUMBER_MAPPING = {
    0 : {'name' : 'Scrypt', 'btc_pwr' : 12},
    1 : {'name' : 'SHA256', 'btc_pwr' : 15},
    2 : {'name' : 'ScryptNf', 'btc_pwr' : 9},
    3 : {'name' : 'X11', 'btc_pwr' : 12},
    4 : {'name' : 'X13', 'btc_pwr' : 9},
    5 : {'name' : 'Keccak', 'btc_pwr' : 12},
    6 : {'name' : 'X15', 'btc_pwr' : 9},
    7 : {'name' : 'Nist5', 'btc_pwr' : 9},
    8 : {'name' : 'NeoScrypt', 'btc_pwr' : 9},
    9 : {'name' : 'Lyra2RE', 'btc_pwr' : 9},
    10 : {'name' : 'WhirlpoolX', 'btc_pwr' : 9},
    11 : {'name' : 'Qubit', 'btc_pwr' : 12},
    12 : {'name' : 'Quark', 'btc_pwr' : 12},
    13 : {'name' : 'Axiom', 'btc_pwr' : 3},
    14 : {'name' : 'Lyra2REv2', 'btc_pwr' : 12},
    15 : {'name' : 'ScryptJaneNf16', 'btc_pwr' : 6},
    16 : {'name' : 'Blake256r8', 'btc_pwr' : 12},
    17 : {'name' : 'Blake256r14', 'btc_pwr' : 12},
    18 : {'name' : 'Blake256r8vnl', 'btc_pwr' : 12},
    19 : {'name' : 'Hodl', 'btc_pwr' : 3},
    20 : {'name' : 'DaggerHashimoto', 'btc_pwr' : 9},
    21 : {'name' : 'Decred', 'btc_pwr' : 12},
    22 : {'name' : 'CryptoNight', 'btc_pwr' : 6},
    23 : {'name' : 'Lbry', 'btc_pwr' : 12},
    24 : {'name' : 'Equihash', 'btc_pwr' : 6},
    25 : {'name' : 'Pascal', 'btc_pwr' : 12},
    26 : {'name' : 'X11Gost', 'btc_pwr' : 9},
    27 : {'name' : 'Sia', 'btc_pwr' : 12},
    28 : {'name' : 'Blake2s', 'btc_pwr' : 12},
    29 : {'name' : 'Skunk', 'btc_pwr' : 9}
}


# #
# PUBLIC FUNCTIONS
# #

'''
Stops nicehash program (kills it) along with supporting files

param nicehash_exe_name      : (optional) can set unique nicehash exe name to kill
param excavator_exe_name     : (optional) can set unique excavator exe name to kill
param xmr_stack_cpu_exe_name : (optional) can set unique xmr_stack_cpu exe name to kill

return                       : 0 on success, 1 on already closed, 2 on error
return type                  : int
'''
def stop_nicehash(nicehash_exe_name = "NiceHash Miner 2.exe", excavator_exe_name = "excavator.exe", xmr_stack_cpu_exe_name = "xmr-stak-cpu.exe"):
    if is_nicehash_miner_open() is False:
        return 1
    try:
        subprocess.call(['TASKKILL', '/F', '/IM', nicehash_exe_name])
        subprocess.call(['TASKKILL', '/F', '/IM', excavator_exe_name])
        subprocess.call(['TASKKILL', '/F', '/IM', xmr_stack_cpu_exe_name])
        return 0
    except Exception as e:
        _logger.error("Failed to stop nicehash.")
        _logger.error(e)
        return 2


'''
Starts nicehash program

param nicehash_exe_path : (optional) full path to the nicehash exe
param nicehash_exe_name : (optional) name of the nicehash exe to execute

return                  : 0 on success, 1 on already open, 2 on error
return type             : int
'''
def start_nicehash(nicehash_exe_path = 'C:\\Users\\mkuka\\AppData\\Local\\Programs\\NiceHash Miner 2', nicehash_exe_name = "NiceHash Miner 2.exe"):
    try:
        if is_nicehash_miner_open() is True:
            return 1
        os.chdir(nicehash_exe_path)
        subprocess.Popen(['NiceHash Miner 2.exe'])
    except Exception as e:
        _logger.error("Failed to start nicehash.")
        _logger.error(e)
        return 2


'''
Checks if nicehash miner is open

param nicehash_exe_name : (optional) name of the nicehash exe

return                  : true if open, false if closed
return type             : bool
'''
def is_nicehash_miner_open(nicehash_exe_name = "NiceHash Miner 2.exe"):
    tasks = ""
    try:
        tasks, errors = subprocess.Popen(['tasklist'], stdout=subprocess.PIPE).communicate()
        tasks = str(tasks)
    except Exception as e:
        _logger.error("Failed to check if nicehash miner is open")
        _logger.error(e)
        return False
    if nicehash_exe_name in tasks:
        return True
    else:
        return False

'''
Gets the current confirmed and pending bitcoin balance in nicehash

param api_id   : api id tied to given user
param api_key  : API Key or API ReadOnly key (prefer readonly for security) tied to the same user
param btc_addr : nicehash wallet bitcoin address

return         : dictionary of format {'balance_confirmed': 0.000, 'balance_pending' : 0.000, "mining_balance" : 0.000}
return type    : dict
'''
def get_current_bitcoin_balance(api_id, api_key, btc_addr):
    resultDict = {
        "balance_confirmed" : 0.0,
        "balance_pending" : 0.0,
        "mining_balance" : 0.0
    }

    # first queries api for user's balance pending and confirmed
    pendingConfirmedBalanceDict = get_pending_and_confirmed_btc_balance(api_id, api_key)
    if pendingConfirmedBalanceDict is None:
        _logger.error("Failed to get pending and confirmed btc balance.")
        return None
    else:
        resultDict['balance_confirmed'] = pendingConfirmedBalanceDict['balance_confirmed']
        resultDict['balance_pending'] = pendingConfirmedBalanceDict['balance_pending']

    # now queries the unclaimed balance from mining
    mining_balance = get_total_mining_balance(btc_addr)
    if mining_balance is None:
        _logger.error("Failed to get mining balance.")
        return None
    else:
        resultDict['mining_balance'] = mining_balance

    return resultDict

'''
Gets the total mining balance (balance that has not been uploaded to the wallet yet)

param btc_addr : nicehash wallet bitcoin address

return         : total mining bitcoin balance
return type    : float
'''
def get_total_mining_balance(btc_addr):
    # fetches dictionary of mining balance per algorithm
    result = _get_api_query(method = STATS_METHOD, arg_dict = {"addr" : btc_addr})
    if result is None:
        _logger.error("Failed to get total mining balance from api.")
        return None
    else:
        # iterates across result algorithms to get the total mining balance
        totalBalance = 0.0
        if 'result' in result and 'stats' in result['result']:
            for stat in result['result']['stats']:
                if 'balance' in stat:
                    totalBalance += float(stat['balance'])
                else:
                    _logger.warn("Invalid stat " + str(stat))
                    continue # skips invalid stats
        else:
            _logger.info("No stats to parse with balance. Mining balance is 0")
        return totalBalance


'''
Gets the pending and confirmed btc balance (balance in the wallet)

param api_id  : api id tied to given user
param api_key : API Key or API ReadOnly key (prefer readonly for security) tied to the same user

return        : dictionary of format {'balance_pending' : 0.0, 'balance_confirmed' : 0.0} or None on error
return type   : dict
'''
def get_pending_and_confirmed_btc_balance(api_id, api_key):
    resultDict = {
        'balance_pending' : 0.0,
        'balance_confirmed' : 0.0
    }

    # fetches dictionary from API
    result = _get_api_query(method = BALANCE_METHOD, arg_dict = {"id" : api_id, "key" : api_key})
    if result is None:
        _logger.error("Failed to get pending and confirmed btc balance from api.")
        return None
    else:
        # populates return dictionary with pending and confirmed balance info
        if 'result' in result:
            resultDict['balance_pending'] = float(result['result']['balance_pending'])
            resultDict['balance_confirmed'] = float(result['result']['balance_confirmed'])
            return resultDict
        else:
            _logger.error("Got invalid data from the api of format " + str(result))
            return None


'''
Gets the maximum profitibility in btc based on current payrates

return      : maximum profitability currently in btc
return type : float, or None on error
'''
def get_maximum_profitability_current():
    # gets the current hashrate per algorithm from the nicehash config files
    config_hashrate_dict = _get_mining_speed_per_algorithm(config_folder_path = _get_config_folder_path())
    if config_hashrate_dict is None:
        _logger.error("Could not get config_dict to calculate maximum profitability current")
        return None

    # gets the current payment per hashrate from the api
    current_payment_dict = _get_api_query(method = CURRENT_STATS_METHOD)
    if config_hashrate_dict is None:
        _logger.error("Could not get current_payment_dict from api to calculate maximum profitability current")
        return None

    # calculates the maximum profitability per algorithm
    profit_per_algo_dict = _calculate_profitability_per_algorithm(config_hashrate_dict, current_payment_dict)
    if profit_per_algo_dict is None:
        _logger.error("Could not calculate profit per algorithm")
        return None

    # returns maximum profitability overall
    maxVal = 0.0
    for profitEntry in profit_per_algo_dict:
        if profit_per_algo_dict[profitEntry] > maxVal:
            maxVal = profit_per_algo_dict[profitEntry]

    return maxVal

'''
Gets the maximum profitibility in btc based on 24h average payrates

'''
def get_maximum_profitability_24h():
    # gets the current hashrate per algorithm from the nicehash config files
    config_hashrate_dict = _get_mining_speed_per_algorithm(config_folder_path = _get_config_folder_path())
    if config_hashrate_dict is None:
        _logger.error("Could not get config_dict to calculate maximum profitability current")
        return None

    # gets the current payment per hashrate from the api
    current_payment_dict = _get_api_query(method = TWENTYFOUR_STATS_METHOD)
    if config_hashrate_dict is None:
        _logger.error("Could not get current_payment_dict from api to calculate maximum profitability current")
        return None

    # calculates the maximum profitability per algorithm
    profit_per_algo_dict = _calculate_profitability_per_algorithm(config_hashrate_dict, current_payment_dict)
    if profit_per_algo_dict is None:
        _logger.error("Could not calculate profit per algorithm")
        return None

    # returns maximum profitability overall
    maxVal = 0.0
    for profitEntry in profit_per_algo_dict:
        if profit_per_algo_dict[profitEntry] > maxVal:
            maxVal = profit_per_algo_dict[profitEntry]

    return maxVal

# #
# PRIVATE FUNCTIONS
# #

'''
Handles querying the nicehash api and returning a JSON dictionary of the results

API Format:
BASE_URL + "method=" + method + "&arg1=" + arg1 + "&arg2=" + arg2...

param method   : API method
param arg_dict : (optional) dictionary of argument key-value pairs (e.g. {"addr" : btc_addr})
param base_url : (optional) base url of the API to use

return         : JSON dictionary of the response, or None on error
return type    : dict, or None
'''
def _get_api_query(method, arg_dict = {}, base_url = API_BASE_URL):
    # builds url
    request_url = API_BASE_URL + "method=" + method
    for arg in arg_dict:
        request_url += "&" + arg + "=" + arg_dict[arg]
    # handle JSON request
    try:
        response = urllib.request.urlopen(request_url).read()
        resultDict = json.loads(response) # parses to dict
        # handles API error in JSON
        if 'result' in resultDict and 'error' in resultDict['result']:
            _logger.error("Error in api query using url " + request_url)
            _logger.error(resultDict['result']['error'])
            return None
        else:
            return resultDict
    except urllib.error.HTTPError as e:
        _logger.error("Failed to get from api using url " + request_url)
        _logger.error("Response Code: " + str(e.code) + " Message: " + str(e))
        return None
    except Exception as e:
        _logger.error("Failed to get from api using url " + request_url)
        _logger.error(e)
        return None


'''
Gets the mining speed per algorithm calculated from the benchmark files generated by nicehash

Note - ensure that the benchmarks were completed first, or we cannot calculate the estimated mining speed using this method

param config_folder_path    : path to the configuration files for nicehash (see _get_config_folder_path())
param general_config_file   : (optional) name of the configuration file
param benchmark_file_header : (optional) name of the header before the benchmark file (filename is header + device_uuid + .json)

return                      : dictionary of the format {'algorithm_string_id' : hashvalue (floating point number)}
return type                 : dict
'''
def _get_mining_speed_per_algorithm(config_folder_path, general_config_file = "General.json", benchmark_file_header = "benchmark_"):
    output_dict = {}
    '''
    output_dict = {
        'algorithm_string_id' : hashvalue (float, e.g. 10002.24024)
    }
    '''

    # opens the General.json file and gets the LastDevicesSettup
    device_uuid_list = []
    try:
        config_dict = json.load(open(config_folder_path + "\\" + general_config_file))
        if 'LastDevicesSettup' not in config_dict:
            _logger.error("Could not find LastDevicesSettup in config file.")
            return None
        for device in config_dict['LastDevicesSettup']:
            if 'UUID' in device:
                device_uuid_list.append(device['UUID'])
            else:
                _logger.warn("Could not get uuid from device " + str(device))

    except Exception as e:
        _logger.error("Failed to read " + general_config_file)
        _logger.error(e)
        return None

    # for each device, opens its benchmark file if it exists and gets the hash speeds
    for device_uuid in device_uuid_list:
        try:
            device_benchmark_dict = json.load(open(config_folder_path + "\\" + benchmark_file_header + device_uuid + ".json"))
            if 'AlgorithmSettings' in device_benchmark_dict:
                for alg_setting in device_benchmark_dict['AlgorithmSettings']:
                    # creates entry in dictionary if it does not already exist
                    if alg_setting['AlgorithmStringID'] not in output_dict:
                        output_dict[alg_setting['AlgorithmStringID']] = 0.0
                    # adds hash speed here  
                    # averages across the benchark modes
                    averageSpeed = 0.0
                    for bm in alg_setting['BenchmarkModes']:
                        averageSpeed += float(bm['Speed'][0])
                    averageSpeed = averageSpeed / len(alg_setting['BenchmarkModes'])
                    output_dict[alg_setting['AlgorithmStringID']] += averageSpeed
        except Exception as e:
            _logger.error("Failed to read " + benchmark_file_header + device_uuid + ".json")
            _logger.error(e)
            return None

    # returns the dictionary of mining speed per algorithm
    return output_dict


'''
Calculates the profitability of each algorithm given an input config dict and api query dict

param config_dict    : configuration dictionary from _get_mining_speed_per_algorithm()
param api_query_dict : api stat dict from CURRENT_STATS_METHOD or 24H_STATS_METHOD (gives speed, price, and algo #)

return               : dictionary mapping algo #s to their calculated profitability in btc
return type          : dict
'''
def _calculate_profitability_per_algorithm(config_dict, api_query_dict):
    result_dict = {}
    # creates result dictionary mapping each algorithm number originally to 0
    for algo_number in ALGORITHM_NUMBER_MAPPING:
        result_dict[algo_number] = 0.0


    # goes through the config dict and calculates the profitability
    for alg_string in config_dict:
        # finds a matching algorithm number
        algNumber = -1
        for algo_number in ALGORITHM_NUMBER_MAPPING:
            if ALGORITHM_NUMBER_MAPPING[algo_number]['name'].lower() in alg_string.split('_')[-1].lower(): # looks for substring past last '_'
                algNumber = algo_number
        if algNumber == -1:
            _logger.warn("Could not find algorithm number for string " + alg_string)
            continue
        # gets required calculation variables from the api_query_dict
        priceVal = 0.0
        priceUnit = ALGORITHM_NUMBER_MAPPING[algNumber]['btc_pwr']
        configSpeed = config_dict[alg_string]
        if 'result' in api_query_dict and 'stats' in api_query_dict['result']:
            for stat in api_query_dict['result']['stats']:
                if int(stat['algo']) == algNumber:
                    priceVal = float(stat['price'])
        else:
            _logger.error("Invalid api query dict.")
            return None
        # calculates profitability using the hash value for that alg number
        result_dict[algNumber] = (priceVal / (math.pow(10,priceUnit))) * configSpeed

    return result_dict


'''
Gets the unique configuration folder path

return      : string path to the configuration folder
return type : string
'''
def _get_config_folder_path():
    # XXX TODO - see if cleaner method to do this
    base_path_footer = r"AppData\Roaming\nhm2\configs"
    username = str(os.path.expanduser("~"))

    return (username + "\\" + base_path_footer)

