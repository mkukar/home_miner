# #
# 
# config_handler.py
#
# Handles getting and saving configuration data
#
#
# Copyright 2018 Michael Kukar
# 
# #

import json
import os
import logging
import datetime
import hashlib
import uuid
import threading

class ConfigHandler():

    # #
    # PUBLIC VARIABLES
    # #
    CONFIG_FILE_FOLDER = str(os.path.expanduser("~")) + r"\AppData\Local\home_miner"
    CONFIG_FILE_NAME = "config.json"

    # #
    # PRIVATE VARIABLES
    # #
    _config_data = None

    _lock = threading.Lock()

    # power usage hourly is kWh power usage (default to national average of 12 cents)
    # mining blackout times is times to not mine (default to 10pm to 7am for sleeping)
    # min profitability is used to determine minimum profitability in USD to start mining
    # min mining time minutes is the minimum time to be mining before closing even if unprofitable
    _TEMPLATE_CONFIG_DATA = {
        'power_usage_hourly' : {
            0 : 0.12,
            1 : 0.12,
            2 : 0.12,
            3 : 0.12,
            4 : 0.12,
            5 : 0.12,
            6 : 0.12,
            7 : 0.12,
            8 : 0.12,
            9 : 0.12,
            10 : 0.12,
            11 : 0.12,
            12 : 0.12,
            13 : 0.12,
            14 : 0.12,
            15 : 0.12,
            16 : 0.12,
            17 : 0.12,
            18 : 0.12,
            19 : 0.12,
            20 : 0.12,
            21 : 0.12,
            22 : 0.12,
            23 : 0.12
        },
        'mining_blackout_times' : [],
        'btc_address' : '',
        'api_id' : '',
        'api_key' : '',
        'estimated_power_usage' : 0.25,
        'min_profitability' : 0.0,
        'min_mining_time_minutes' : 10
    }

    __s = "P!zZ@D035N*tN33dS@lt!" # salt for encryption

    # #
    # CONSTRUCTOR
    # #

    '''
    Basic constructor to initially read in config file data to memory.

    If the file does not already exist, creates an empty template config file.

    '''
    def __init__(self):
        # sets up logger
        self._logger = logging.getLogger(__name__)
        self._logger.setLevel(logging.INFO)
        ch = logging.StreamHandler() # XXX TODO - move from console to file after debugging fully complete
        ch.setLevel(logging.DEBUG)
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        ch.setFormatter(formatter)
        self._logger.addHandler(ch)

        # creates config file filepath if it does not already exist
        if not os.path.isdir(self.CONFIG_FILE_FOLDER):
            try:
                os.mkdir(self.CONFIG_FILE_FOLDER)
            except Exception as e:
                self._logger.error("Failed to create config file folder " + self.CONFIG_FILE_FOLDER)
                self._logger.error(e)

        # create template if required
        if not os.path.isfile(self.CONFIG_FILE_FOLDER + "\\" + self.CONFIG_FILE_NAME):
            # if not, create template version
            self._generate_template_config()
        
        # loads the data
        self.load_data()
            


    # #
    # PUBLIC FUNCTIONS
    # #


    '''
    Calculates the current kWh price of electricity

    return      : current price in electricity in kWh
    return type : float or None on error
    '''
    def get_current_power_price(self):
        # gets current hour of the day
        now = datetime.datetime.now()
        # gets pricing for this hour
        return self.get_power_price(hour = now.hour)


    '''
    Calculates the price of electricity given the hour

    param hour  : hour of the day to get the price

    return      : price in electricity in kWh
    return type : float or None on error
    '''
    def get_power_price(self, hour):
        if hour > 23 or hour < 0:
            self._logger.error("Invalid hour given " + str(hour))
            self._logger.error("Cannot calculate current power price.")
            return None
        with self._lock:
            return float(self._config_data['power_usage_hourly'][str(hour)])


    '''
    Sets the power price for the given list of hours to the set price

    param hourList : list of hours
    param price    : price in kWh
 
    return         : true on success, false on fail
    return type    : bool
    '''
    def set_power_price(self, hourList, price):
        with self._lock:
            # sets the price per hour
            for hour in hourList:
                if hour > 23 or hour < 0:
                    self._logger.warn("Cannot save price for hour of value " + str(hour))
                    self._logger.warn("Hour must be between 0 and 23")
                    continue
                self._config_data['power_usage_hourly'][str(hour)] = float(price)

        # saves the data
        self.save_data()
        return True


    '''
    Saves the required API information

    param api_key : api readonly key
    param api_id  : api id

    return        : true on success, false on fail
    return type   : bool
    '''
    def set_api_data(self, api_key, api_id):
        with self._lock:
            self._config_data['api_id'] = str(api_id)
            self._config_data['api_key'] = str(api_key)
        return self.save_data()


    '''
    Saves the bitcoin address

    param btc_address : btc address of nicehash wallet

    return            : true on success, false on fail
    return type       : bool
    '''
    def set_btc_address(self, btc_address):
        with self._lock:
            self._config_data['btc_address'] = str(btc_address)
        return self.save_data()


    '''
    Saves the configuration data to the file

    return      : true on success, false on fail
    return type : bool
    '''
    def save_data(self):
        with self._lock:
            # saves the data
            try:
                with open(self.CONFIG_FILE_FOLDER + "\\" + self.CONFIG_FILE_NAME, 'w') as config_file:
                    json.dump(self._config_data, config_file)
                    return True
            except Exception as e:
                self._logger.error("Failed to write template config file.")
                self._logger.error(e)
                return False


    '''
    Loads the configuration data into memory

    return      : true on success, false on fail
    return type : bool
    '''
    def load_data(self):
        with self._lock:
            # loads the data again, overwriting any current changes
            try:
                self._config_data = json.load(open(self.CONFIG_FILE_FOLDER + "\\" + self.CONFIG_FILE_NAME))
                return True
            except Exception as e:
                _logger.error("Failed to read configuration file.")
                return False


    '''
    GENERAL GETTERS FROM CONFIG FILE
    '''
    def get_api_key(self):
        with self._lock:
            return self._config_data['api_key']


    def get_api_id(self):
        with self._lock:
            return self._config_data['api_id']


    def get_btc_address(self):
        with self._lock:
            return self._config_data['btc_address']


    def get_mining_blackout_times(self):
        with self._lock:
            return self._config_data['mining_blackout_times']


    def get_minimum_profitability(self):
        with self._lock:
            return float(self._config_data['min_profitability'])


    def get_minimum_mining_time(self):
        with self._lock:
            return int(self._config_data['min_mining_time_minutes'])


    def get_estimated_power_usage(self):
        with self._lock:
            return float(self._config_data['estimated_power_usage'])
        
    # #
    # PRIVATE FUNCTIONS
    # #

    '''
    Generates a template configuration file

    param force : (optional) if true, will always generate the config file regardless if it one exists

    return      : true on success, false on fail
    return type : bool
    '''
    def _generate_template_config(self, force = False):
        with self._lock:
            # makes sure file does not already exist (if no force)
            if force or not os.path.isfile(self.CONFIG_FILE_FOLDER + "\\" + self.CONFIG_FILE_NAME):
                try:
                    with open(self.CONFIG_FILE_FOLDER + "\\" + self.CONFIG_FILE_NAME, 'w') as config_file:
                        json.dump(self._TEMPLATE_CONFIG_DATA, config_file)
                    return True
                except Exception as e:
                    self._logger.error("Failed to write template config file.")
                    self._logger.error(e)
                    return False
            else:
                self._logger.warn("Config file already exists. Not writing template.")
                return False

