# #
# 
# config_window.py
#
# window to edit the config file from the gui
#
# Copyright 2018 Michael Kukar
# 
# #

from tkinter import Tk, Button, StringVar, Label, Entry, Frame


from config_handler import ConfigHandler

class ConfigWindow(Frame):

    # #
    # PUBLIC VARIABLES
    # #

    # #
    # PRIVATE VARIABLES
    # #


    # #
    # CONSTRUCTOR
    # #

    def __init__(self, master):
        Frame.__init__(self, master)
        self.root = master

        # loads config
        self.config = ConfigHandler()

        # sets up gui display for the main window
        master.title("CONFIG EDITOR")
        master.configure(bg = "white")
        master.resizable(width = False, height = False)

        # variables
        self.btc_addr_text = StringVar()
        self.api_id_text = StringVar()
        self.api_key_text = StringVar()
        self.min_profit_text = StringVar()
        self.min_mining_time_text = StringVar()
        self.est_power_usage_text = StringVar()
        self.power_cost_text = StringVar()
        self.info_label_text = StringVar()

        # info label
        self.info_label = Label(master, textvariable = self.info_label_text, bg = 'white', font = 'helvetica 10')
        self.info_label.grid(row = 0, column = 1, sticky = 'news', padx = 5, pady = 5)
        self.info_label_text.set("")

        # save button
        self.save_button = Button(master, text = "SAVE", command = self.save_data, font = "helvetica 10")
        self.save_button.grid(row = 0, column = 0, columnspan = 2, sticky = 'w', padx = 5, pady = 5)

        # labels and their associated fields
        self.btc_addr_label = Label(master, text = "BTC Address: ", bg = 'white', font = 'helvetica 10 bold')
        self.btc_addr_label.grid(row = 2, column = 0, sticky = 'e', padx = 5, pady = 5)
        self.btc_addr_entry = Entry(master, textvariable = self.btc_addr_text, font = 'helvetica 10', width = 50)
        self.btc_addr_entry.grid(row = 2, column = 1, sticky = 'news', padx = 5, pady = 5)

        self.api_id_label = Label(master, text = "API ID: ", bg = 'white', font = 'helvetica 10 bold')
        self.api_id_label.grid(row = 3, column = 0, sticky = 'e', padx = 5, pady = 5)
        self.api_id_entry = Entry(master, textvariable = self.api_id_text, font = 'helvetica 10', width = 50)
        self.api_id_entry.grid(row = 3, column = 1, sticky = 'news', padx = 5, pady = 5)

        self.api_key_label = Label(master, text = "API Key: ", bg = 'white', font = 'helvetica 10 bold')
        self.api_key_label.grid(row = 4, column = 0, sticky = 'e', padx = 5, pady = 5)
        self.api_key_entry = Entry(master, textvariable = self.api_key_text, font = 'helvetica 10', width = 50)
        self.api_key_entry.grid(row = 4, column = 1, sticky = 'news', padx = 5, pady = 5)

        self.min_profit_label = Label(master, text = "Min. Profit ($/Hr): ", bg = 'white', font = 'helvetica 10 bold')
        self.min_profit_label.grid(row = 5, column = 0, sticky = 'e', padx = 5, pady = 5)
        self.min_profit_entry = Entry(master, textvariable = self.min_profit_text, font = 'helvetica 10', width = 50)
        self.min_profit_entry.grid(row = 5, column = 1, sticky = 'news', padx = 5, pady = 5)

        self.power_cost_label = Label(master, text = "Power Cost ($/kWh): ", bg = 'white', font = 'helvetica 10 bold')
        self.power_cost_label.grid(row = 6, column = 0, sticky = 'e', padx = 5, pady = 5)
        self.power_cost_entry = Entry(master, textvariable = self.power_cost_text, font = 'helvetica 10', width = 50)
        self.power_cost_entry.grid(row = 6, column = 1, sticky = 'news', padx = 5, pady = 5)

        # loads variables from config
        self.load_data()


    # #
    # FUNCTIONS
    # #


    '''
    Loads data from the config handler into the variable fields
    '''
    def load_data(self):
        # loads data into memory
        self.config.load_data()

        # populates stringvar fields
        self.btc_addr_text.set(self.config.get_btc_address())
        self.api_id_text.set(self.config.get_api_id())
        self.api_key_text.set(self.config.get_api_key())
        self.min_profit_text.set(self.config.get_minimum_profitability())
        self.power_cost_text.set(self.config.get_power_price(0)) # gets one index as we set all of them the same



    '''
    Saves data from the variable fields into the config handler
    '''
    def save_data(self):
        self.config.set_api_data(
            self.api_key_text.get(),
            self.api_id_text.get()
            )
        self.config.set_btc_address(self.btc_addr_text.get())
        try:
            self.config._config_data['min_profitability'] = float(self.min_profit_text.get())
        except Exception as e:
            self.info_label_text.set("Save Failed! Minimum profitability must be a floating point number.")
            self.info_label.configure(foreground = "red")
            return
        self.config.set_power_price(range(0,24), self.power_cost_text.get())
        self.config.save_data()
        self.info_label_text.set("Saved.")
        self.info_label.configure(foreground = "green")
