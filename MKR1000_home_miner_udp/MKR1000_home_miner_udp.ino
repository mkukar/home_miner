#include "arduino_secrets.h"
/*
  MKR1000_home_miner_udp
  
  This sketch reads wattage and power information from the Arduino
    and then sends it back over UDP to the main server
    
  Copyright 2018 Michael Kukar - All rights reserved.

 */


#include <SPI.h>
#include <WiFi101.h>
#include <WiFiUdp.h>

const int sensorPin = A1; // power sensor pin

// power sensor variables
int mVperAmp = 66;
double Voltage = 0;
double VRMS = 0;
double AmpsRMS = 0;

int status = WL_IDLE_STATUS;
///////please enter your sensitive data in the Secret tab/arduino_secrets.h
char ssid[] = SECRET_SSID;        // your network SSID (name)
char pass[] = SECRET_PASS;    // your network password (use for WPA, or use as key for WEP)
int keyIndex = 0;            // your network key Index number (needed only for WEP)

unsigned int localPort = 2390;      // local port to listen on

char packetBuffer[255]; //buffer to hold incoming packet
char ReplyBuffer[255]; // buffer to hold response

char SEARCH_MESSAGE[] = "ARDUINO_SEARCH";
char SEARCH_RESPONSE[] = "ARDUINO_FOUND";       // a string to send on search


char TEMP_QUERY_HEADER[] = "ARDUINO_TEMP_"; // ARDUINO_TEMP_F or ARDUINO_TEMP_C
char TEMP_RESPONSE_HEADER[] = "TEMP_"; // TEMP_75_F or TEMP_10_C for temp in c or f

char POWER_QUERY[] = "ARDUINO_POWER";
char POWER_RESPONSE_HEADER[] = "POWER_"; // POWER_## e.g. POWER_120 for power in watts

char CELCIUS = 'C';
char FARENHEIT = 'F';


WiFiUDP Udp;

void setup() {
  //Initialize serial and wait for port to open:
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }

  // check for the presence of the shield:
  if (WiFi.status() == WL_NO_SHIELD) {
    Serial.println("WiFi shield not present");
    // don't continue:
    while (true);
  }

  // attempt to connect to WiFi network:
  while ( status != WL_CONNECTED) {
    Serial.print("Attempting to connect to SSID: ");
    Serial.println(ssid);
    // Connect to WPA/WPA2 network. Change this line if using open or WEP network:
    status = WiFi.begin(ssid, pass);

    // wait 10 seconds for connection:
    delay(10000);
  }
  Serial.println("Connected to wifi");
  printWiFiStatus();

  Serial.println("\nStarting connection to server...");
  // if you get a connection, report back via serial:
  Udp.begin(localPort);
}

void loop() {

  // if there's data available, read a packet
  int packetSize = Udp.parsePacket();
  if (packetSize)
  {
    Serial.print("Received packet of size ");
    Serial.println(packetSize);
    Serial.print("From ");
    IPAddress remoteIp = Udp.remoteIP();
    Serial.print(remoteIp);
    Serial.print(", port ");
    Serial.println(Udp.remotePort());

    // read the packet into packetBufffer
    int len = Udp.read(packetBuffer, 255);
    if (len > 0) packetBuffer[len] = 0;
    Serial.println("Contents:");
    Serial.println(packetBuffer);

    // does memcopy for substring temp
    char headerPacket[1024];
    memcpy(headerPacket, packetBuffer, strlen(TEMP_QUERY_HEADER));

    if (strcmp(packetBuffer, SEARCH_MESSAGE) == 0) {
      // search message, so send back REPLY_MESSAGE
      strcpy(ReplyBuffer, SEARCH_RESPONSE);
    }
    else if (strcmp(packetBuffer, POWER_QUERY) == 0) {
      // power usage query
      
      // builds response string
      strcpy(ReplyBuffer, POWER_RESPONSE_HEADER);
      char powerStr[10];
      itoa(readWatts(), powerStr, 10); // converts int to string
      strcat(ReplyBuffer, powerStr);
    }
    else if (strcmp(headerPacket, TEMP_QUERY_HEADER)) {
      // gets units from string
      char units = packetBuffer[strlen(TEMP_QUERY_HEADER) + 1];
      
      // assembles response string
      strcpy(ReplyBuffer, TEMP_RESPONSE_HEADER);
      char tempStr[10];
      itoa(readTemp(units), tempStr, 10); // converts int to string
      strcat(ReplyBuffer, tempStr);
    }

    // send a reply, to the IP address and port that sent us the packet we received
    Udp.beginPacket(Udp.remoteIP(), Udp.remotePort());
    Udp.write(ReplyBuffer);
    Udp.endPacket();
    Serial.print("REPLY: ");
    Serial.println(ReplyBuffer);
  }
}


void printWiFiStatus() {
  // print the SSID of the network you're attached to:
  Serial.print("SSID: ");
  Serial.println(WiFi.SSID());

  // print your WiFi shield's IP address:
  IPAddress ip = WiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);

  // print the received signal strength:
  long rssi = WiFi.RSSI();
  Serial.print("signal strength (RSSI):");
  Serial.print(rssi);
  Serial.println(" dBm");
}

/*
 Reads in the current expended wattage

 return      : Watts of power being used currently
 return type : int
*/
int readWatts() {
  // requires 1 second to read
  Voltage = getVoltage();
  VRMS = (Voltage/2.0) * 0.707;
  AmpsRMS = (VRMS * 1000)/mVperAmp;
  
  return int(AmpsRMS * 120.0); // 120V power
}

// adapted from http://henrysbench.capnfatz.com/henrys-bench/arduino-current-measurements/acs712-arduino-ac-current-tutorial/
float getVoltage() {
  float result;
  
  int readValue;
  int maxValue = 0; 
  int minValue = 1024;
  
   uint32_t start_time = millis();
   while((millis()-start_time) < 1000) // 1 second sample
   {
       readValue = analogRead(sensorPin);
       if (readValue > maxValue) 
       {
           maxValue = readValue;
       }
       if (readValue < minValue) 
       {
           minValue = readValue;
       }
   }
   
   result = ((maxValue - minValue) * 5.0)/1024.0;
      
   return result;

}

/*
 Reads in the current temperature
 
 param units : FARENHEIT or CELCIUS
 
 return      : temperature in F or C
 return type : int
*/
int readTemp(char units) {
  // XXX TODO
  return 72;
}


